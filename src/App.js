import React, {
  Component
} from 'react';
import './App.css';

var buttonPadding = {"padding-top": "10px" };
var screenPadding = { "border-style":"solid", "width" : "15%", "height": "30px"};


export default class App extends Component {
  state = {
    hello: "Calculator",
    screen: "",
    operation: "",
    prev:""
  }


  // number buttons in screen
  // arrow functions 
  // - cleaner, shorter code
  // - assign ka ng function sa variable
  // - (Variable Name) = (Inside varibale) => { function }
  // - In React, kumukuha ng events para mag trigger ng functions (cause and effect)
  // event(onClick).target(button may zero).value

  // setState() - function, may argument na object which is ung value na ichachange mo state
  // "getters and setters"
  numberButton = event => {
    this.setState({screen: this.state.screen + event.target.value})
    
  }

  clearButton = event => {
    this.setState({screen: ""})

  }

  bkButton = event => {
    this.setState({screen: this.state.screen.toString().slice(0, -1)})
  }

  addButton = event =>{
    this.setState({operation: "add", prev: this.state.screen, screen: ""})
  }

  subtractButton = event =>{
    this.setState({operation: "subtract", prev: this.state.screen, screen: ""})
  }

  multiplyButton = event =>{
    this.setState({operation: "multiply", prev: this.state.screen, screen: ""})
  }

  divideButton = event =>{
    this.setState({operation: "divide", prev: this.state.screen, screen: ""})
  }

  equalsButton = event => {
    if(this.state.operation === "add"){
      this.setState({screen: parseInt(this.state.prev) + parseInt(this.state.screen)})
    }
   else if(this.state.operation === "subtract"){
     this.setState({screen: parseInt(this.state.prev) - parseInt(this.state.screen)})
   }
   else if(this.state.operation === "divide"){
    this.setState({screen: parseInt(this.state.prev) / parseInt(this.state.screen)})
  }
  else if(this.state.operation === "multiply"){
    this.setState({screen: parseInt(this.state.prev) * parseInt(this.state.screen)})
  }
  }
  

  render() {
    return ( 
    <div >
      <h1>{this.state.hello} </h1>
      <h3 style={screenPadding} >{this.state.screen} </h3>

      {/* Numbers */}
      <div>
        <button value="0" onClick={this.numberButton} > 0 </button>
        <button value="1" onClick={this.numberButton}> 1 </button>
        <button value="2" onClick={this.numberButton}> 2 </button>
        <button value="3" onClick={this.numberButton}> 3 </button>
        <button value="4" onClick={this.numberButton}> 4 </button>
        <button value="5" onClick={this.numberButton}> 5 </button>
        <button value="6" onClick={this.numberButton}> 6 </button>
        <button value="7" onClick={this.numberButton}> 7 </button>
        <button value="8" onClick={this.numberButton}> 8 </button>
        <button value="9" onClick={this.numberButton}> 9 </button>
      </div> 
       {/* Operations */}
       <div style={buttonPadding}>
         <button value="add" onClick={this.addButton}> + </button>
         <button value="subtract" onClick={this.subtractButton}> - </button>
         <button value="divide" onClick={this.divideButton}> / </button>
         <button value="multiply" onClick={this.multiplyButton}> x </button>
         <button onClick={this.equalsButton}> = </button>
         <button onClick={this.clearButton}> CLEAR </button>
         <button onClick={this.bkButton}>BK SPACE</button>
      
      </div> 

    </div>
    );
  }
}